package com.citi.training.staff.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.citi.training.staff.model.Employee;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EmployeeControllerTests {
    
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_save_findAll(){
        Employee testEmployee = new Employee();
        testEmployee.setName("Mr Rest Integration Test");
        testEmployee.setAddress("Test Street");
        
        ResponseEntity<Employee> response = restTemplate.postForEntity("/v1/employee", 
                                                                        testEmployee, Employee.class);

        assertEquals(response.getStatusCode(),HttpStatus.CREATED);

    }

}
