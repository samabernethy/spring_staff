package com.citi.training.staff.dao;

import java.util.List;

import com.citi.training.staff.model.Employee;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeMongoRepo extends MongoRepository<Employee, String> {
    
    List<Employee> findByAddress(String address);
}
